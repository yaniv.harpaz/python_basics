import  os
import  datetime
import  uvicorn
from    fastapi import FastAPI

# uvicorn generate_files:app

app = FastAPI()

@app.get("/create_file/")
async def rest_create_file():
    file_name = create_file()
    return {"file_path": file_name}

def generate_text():
    return """
            The Zen of Python, by Tim Peters
            Beautiful is better than ugly.
            Explicit is better than implicit.
            Simple is better than complex.
            Complex is better than complicated.
            """

def get_current_time():
    return datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")

def create_file():
    current_directory   = os.getcwd()
    now_str             = get_current_time()
    file_name           = f"my_data_{now_str}.txt"
    full_file_name      = f"{current_directory}\\generated_data\\{file_name}"

    with open(full_file_name, 'w') as output_file:
        output_file.write(generate_text())

    print(f"Write into {full_file_name} complete")
    return full_file_name
    
def main():
    create_file()

if __name__ == "__main__":
    uvicorn.run("generate_files:app", host="127.0.0.1", port=8000, log_level="info")
   