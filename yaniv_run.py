# %%
for counter in range(10):
    print(counter)
# %%
print(counter)
# %%
my_list = list(range(10000))
# %%
print(my_list) 

# %%
a = 10
b = 20 
# %%
a, b = b, a 
# %%
print(0 < a < 1000)
# %%
my_list 
# %%
my_list = list(range(10))
# %%
my_list[3:5] = [100, 200, 300]
# %%
300 in my_list
# %%
400 in my_list
# %%
my_new_list = [1000, 2000, 3000] 
# %%
my_new_list + my_list 
# %%
3 * my_new_list
# %%
for counter in range(10):
    print('*' * counter)
# %%
type([1,2,3,4])
# %%
type((1,2,3,4))
my_list = [1,2,3,4] 
my_tup  = (1,2,3,4)

# %%
my_list[1:2] = [111,222]
# %%
my_tup[1:2] = (111,222)
# %%
a,b 
# %%
a1, a2, a3, a4, a5 = my_list
# %%
a, b = b,a 
# %%
my_dict = {1: 'Shmulik'
            ,2: 'Rei'
            ,3: 'Yaniv'}
# %%
my_dict[2]
# %%
my_emps = {
    1: {
            'name': 'Shmulik',
            'city': 'Givat_shmuel'
    },
    2: {
        'name': 'Rei',
        'city': 'Ariel'
    },
    3: {
        'name': 'Yaniv',
        'city': 'Kiryat Ono'
    }

}
# %%
my_emps[1]['city']
# %%
for item in my_emps:
    print(item, type(item), my_emps[item])
# %%
my_emps.keys()
# %%
my_emps.values()
# %%
dir(my_dict)
# %%
for index, value in my_emps.items():
    print(index, value)
# %%
len(my_emps)
# %%
my_list
# %%
for index, item in enumerate(my_list):
    if index == 22:
        break
    print(index, item)
    print('***')


# %%
b

# %%
c = 100 if b > 10 else 200
# %%
if b > 10:
    c = 100
else: 
    c = 200


# %%
str(10003234)
# %%
float(30)
# %%
for item in my_dict:
    print("hello {0}, how r u?".format(my_dict[item]))
# %%
